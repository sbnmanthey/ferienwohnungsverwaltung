/*
* Eine mit Nutzeroberfläche versehene Ferienwohnungsverwaltung zum anlegen von Nutzer, hinzufügen und bearbeiten von Wohnungen und der Erstellung von Buchungen.
* Daten werden in 3 CSV Dateien gespeichert.
*
* Klasse: FS63 am OSZIMT
*
* Autoren: Stephan Bieman, Julian Bischop, Sebastian Manthey, Tim Kohlsdorf, Liam Scholz
*
* Version 1.TagDerVorstellung - 02.06.17
*
*
 */
package PfefferPlusTim;

import javafx.application.Application;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.util.*;


public class Main extends Application
{

    private static ArrayList<String> hauser = new ArrayList<>();
    private static ArrayList<String> kunden = new ArrayList<>();
    private static ArrayList<String> buchung = new ArrayList<>();
    private static ArrayList<String> ausgabe = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Ferienwohnungsverwaltung GUI");

        primaryStage.setScene(new Scene(root, 590, 400));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("file:resources/favicon.png"));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static ArrayList<String> getHauser() { return hauser; }
    public static ArrayList<String> getKunden() { return kunden; }
    public static ArrayList<String> getBuchung() { return buchung; }
    public static ArrayList<String> getAusgabe() { return ausgabe; }
}