/*
* Eine mit Nutzeroberfläche versehene Ferienwohnungsverwaltung zum anlegen von Nutzer, hinzufügen und bearbeiten von Wohnungen und der Erstellung von Buchungen.
* Daten werden in 3 CSV Dateien gespeichert.
*
* Klasse: FS63 am OSZIMT
*
* Autoren: Stephan Bieman, Julian Bischop, Sebastian Manthey, Tim Kohlsdorf, Liam Scholz
*
* Version 1.TagDerVorstellung - 02.06.17
*
*
 */
package PfefferPlusTim;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.w3c.dom.ranges.Range;

import java.util.*;
import java.io.*;
import java.nio.file.*;
import java.nio.charset.*;



public class Controller extends Main
{
    // START FXML
    @FXML Label P_einzel; @FXML Label P_alle;
    @FXML ProgressBar einzel_belegung; @FXML ProgressBar alle_belegung;
    @FXML DatePicker Date_choose_begin; @FXML DatePicker Date_choose_end;
    @FXML Tab tab_auswahl; @FXML Tab tab_belegung; @FXML Tab tab_buchung; @FXML Tab tab_register; @FXML Tab tab_ueberarbeiten;
    @FXML ChoiceBox kundenwahl_b; @FXML ChoiceBox hauswahl_a; @FXML ChoiceBox hauswahl_b; @FXML ChoiceBox hauswahl_n; @FXML ChoiceBox hauswahl_g;
    @FXML TextField register_nachname; @FXML TextField register_vorname; @FXML TextField register_adresse; @FXML TextField register_nummer; @FXML TextField wohnung_adresse; @FXML TextField wohnung_flaeche; @FXML TextField wohnung_preis;
    @FXML TextArea ausgabe_area;

    // END FXML
    String getDefaultHauser = "Faumstrasse 23;15m²;20€\nUlassteigerweg 57;15m²;20€\nClesaecke 30;15m²;20€\nKlarockstrasse 35;25m²;30€\nDenzolstrasse 1;25m²;30€\nArsteralle 93;35m²;40€\nHasenberg 29;35m²;40€\nOrisnawall 109;35m²;40€\nErnstrufftor 17;35m²;40€\nSertinweg 12;35m²;40€";


    public void initialize()
    {
        fileCreateDefault();
        readAusgabe();
        readWohnungen();
        readBuchungen();
        onBelegungTab();


    }
    void fileCreateDefault(){
        fileCreator("Wohnungen.csv","Adresse:;Flaeche:;Preis:"+"\n"+getDefaultHauser);
        fileCreator("Buchungen.csv","K-index:;W-index:;Anfang:;Ende:");
        fileCreator("Kunden.csv","Nachname:;Vorname:;Adresse:;Nummer:");
    }
    boolean fileExist(String fileName){
        File f = new File(fileName);
        if(f.exists()){
            return true;
        }
        else{
            return false;
        }
    }
    void fileCreator(String fileName,String firstLine){
        if (!fileExist(fileName)) {
            try{
                PrintWriter writer = new PrintWriter(fileName, "UTF-8");
                writer.print(firstLine);
                writer.close();
            } catch (IOException e) {
                Alerter("Fehler", "Datei konnte nicht erstellt werden");
            }
        }
    }



    private void Alerter(String title, String message)
    {
        Alert popUp = new Alert(Alert.AlertType.INFORMATION);
        popUp.setHeaderText(null);
        popUp.setTitle(title);
        popUp.setContentText(message);
        popUp.showAndWait();
    }
    public void onBelegungTab(){
        readBelegung();
        readBuchungWerte();
        setBelegung_bars_a();
        P_alle.setText(String.format("%.2f",auslastung_alle()*100)+"%");

    }

    double auslastung_alle(){
        double w = 0;
        for (int i = 0; i < getHauser().subList(1, getHauser().size()).size(); i++){
            w = w + auslastung(i);
        }
        return w/ getHauser().subList(1, getHauser().size()).size();
    }

    double auslastung(int w_index){
        int count = 0;                                          //Inizialisierung eines Counters, der bei jeder gefundenen Buchung die Dauer auf den vorherigen Wert addiert
        for(int i = 1;i< getBuchung().size();i++){              //Schleife - häufigkeit: länge der Liste
            String s = getBuchung().get(i);                     //Auslesen der i. Zeile
            s = s.replaceAll("\\s","");       //Whitespace entfernen
            String[] b = s.split(",");                   //Erstellen des WerteArrays aus String
            if(Integer.parseInt(b[1]) == w_index){              // Vergleich ob richtige Wohnung
                int dauer = datetodoY(b[3]) - datetodoY(b[2]);  // Errechnen der Dauer
                count = count + dauer;                          // Dauer zum counter addieren
            }
        }
        return (double) count/364;  //Rückgabe der Auslastung zwischen 0 und 1
    }
    public void btn_belegung_ausgabe(){
        setBelegung_bars_e(auslastung(hauswahl_n.getSelectionModel().getSelectedIndex()));

    }
   public void tab_selection_wrapper(){
        String tab = getSelectedTab();
        switch (tab){
            case "buchung":
                readBuchungen();
                break;
            case "auswahl":
                readAusgabe();
                break;
            case "belegung":
                onBelegungTab();
                break;
            case "register":
                break;
            case "ueberarbeiten":
                readWohnungen();
                break;
            case "0":
                Alerter("Fehler", "Fehler bei tab_selection_wrapper");
                break;

        }
    }

    public void readBuchungWerte()
    {
        getBuchung().clear();
        readFile("Buchungen.csv", getBuchung());
    }


    public String getSelectedTab()
    {
        String AcTab = "0";
        if(tab_buchung.isSelected()) { AcTab =  "buchung";}
        if(tab_auswahl.isSelected()) { AcTab = "auswahl"; }
        if(tab_belegung.isSelected()) { AcTab = "belegung"; }
        if(tab_register.isSelected()) { AcTab = "register"; }
        if(tab_ueberarbeiten.isSelected()) { AcTab = "ueberarbeiten"; }
        if(AcTab.equals("0")){
            Alerter("Fehler", "Fehler beim auslesen des aktuellen Tabs");
        }
        return AcTab;
    }

    void setBelegung_bars_e(double per){
        einzel_belegung.setProgress(per);
        P_einzel.setText(String.format("%.2f",per*100)+"%");
    }
    private void setBelegung_bars_a(){
        alle_belegung.setProgress((auslastung_alle()));
    }

    private int datetodoY(String date)
    {
        try {
            String[] data = date.split("-");
            Calendar calendar = new GregorianCalendar(Integer.parseInt(data[0]),Integer.parseInt(data[1])-1, Integer.parseInt(data[2]));
            int doY = calendar.get(Calendar.DAY_OF_YEAR);
            return doY;
        } catch (NumberFormatException e) {
            Alerter("Fehler","Eingabeformat ungültig - CSV eventuell beschädigt");
        }
        return -1;
    }


    // File Writer
    private void writeFile(String fileInfo, String writeValue)
    {
        try
        {
            FileWriter Writer = new FileWriter(fileInfo, true);
            Writer.write(writeValue);
            Writer.close();
        }

        catch (IOException ex)
        {
            Alerter("Fehler",fileInfo+" konnte nicht geladen werden!");
        }
    }

    // Kunde hinzufügen
    public void writeKunde()
    {
        if(register_nachname.getText().isEmpty() || register_vorname.getText().isEmpty() || register_adresse.getText().isEmpty() || register_nummer.getText().isEmpty())
        {
            Alerter("Benachrichtigung", "Das Hinzufügen des Kunden ist fehlgeschlagen. Es sind nicht alle Felder ausgefüllt!");
        }

        else
        {
            writeFile("Kunden.csv", "\n"
                    + register_nachname.getText() + ";"
                    + register_vorname.getText() + ";"
                    + register_adresse.getText() + ";"
                    + register_nummer.getText());

            Alerter("Benachrichtigung", "Der Kunde wurde erfolgreich hinzugefügt!");

            register_nachname.setText("");
            register_vorname.setText("");
            register_adresse.setText("");
            register_nummer.setText("");
            initialize();
        }
    }

    // Wohnung ueberarbeiten
    public void rewriteWohnung()
    {
        int wohnungsIndex = hauswahl_g.getSelectionModel().getSelectedIndex();

        try
        {
            if(hauswahl_g.getValue() == null)
            {
                Alerter("Benachrichtigung", "Die Bearbeitung ist fehlgeschlagen. Es muss zuerst eine Wohnung ausgewählt werden!");
            }

            else if(wohnung_adresse.getText().isEmpty() || wohnung_flaeche.getText().isEmpty() || wohnung_preis.getText().isEmpty())
            {
                Alerter("Benachrichtigung", "Die Bearbeitung ist fehlgeschlagen. Es sind nicht alle Felder augefüllt!");
            }

            else if(wohnung_adresse.getText().matches("[0-9]*") || !wohnung_flaeche.getText().matches("[0-9]*m²") || !wohnung_preis.getText().matches("[0-9]*€"))
            {
                Alerter("Benachrichtigung", "Die Bearbeitung ist fehlgeschlagen. Die Felder enthalten nicht die richtigen Zeichen!");
            }

            else
            {
                Path filePath = Paths.get("Wohnungen.csv");
                Charset charSet = StandardCharsets.UTF_8;
                String fileData = new String(Files.readAllBytes(filePath), charSet).replaceAll(getHauser().get(wohnungsIndex+1).replace(", ", ";"), wohnung_adresse.getText() + ";"
                + wohnung_flaeche.getText() + ";"
                + wohnung_preis.getText());
                Files.write(filePath, fileData.getBytes(charSet));

                wohnung_adresse.setText("");
                wohnung_flaeche.setText("");
                wohnung_preis.setText("");
                Alerter("Benachrichtigung", "Die Wohnung wurde erfolgreich bearbeitet!");
                initialize();
            }
        }

        catch(IOException ex)
        {
            ex.getStackTrace();
            Alerter("Fehler", "Wohnungen.csv konnte nicht geladen werden");
        }
    }

    // Buchung erstellen
    public void writeBuchung()
    {
        if(kundenwahl_b.getValue() == null || hauswahl_b.getValue() == null || Date_choose_begin.getValue() == null || Date_choose_end.getValue() == null)
        {
            Alerter("Benachrichtigung", "Die Buchung ist fehlgeschlagen. Es sind nicht alle Felder ausgefüllt!");
        }

        else if(Date_choose_begin.getValue().getYear() != Calendar.getInstance().get(Calendar.YEAR) || Date_choose_end.getValue().getYear() != Calendar.getInstance().get(Calendar.YEAR))
        {
            Alerter("Benachrichtigung", "Die Buchung ist fehlgeschlagen. Es kann nur für das aktuelle Jahr gebucht werden!");
        }

        else if(Date_choose_begin.getValue().getDayOfYear() >= Date_choose_end.getValue().getDayOfYear())
        {
            Alerter("Benachrichtigung", "Die Buchung ist fehlgeschlagen. Es kann im Kalenderjahr nur vorwärts gebucht werden!");
        }

        else if(!isFree(hauswahl_b.getSelectionModel().getSelectedIndex(), Date_choose_begin.getValue().toString(),Date_choose_end.getValue().toString()))
        {
            Alerter("Benachrichtigung", "Die Buchung ist fehlgeschlagen. Es ist schon eine Buchung dieser Wohnung für den gewählten Zeitraum vorhanden!");
        }

        else
        {
            if(Date_choose_begin.getValue().getDayOfMonth() <= Date_choose_end.getValue().getDayOfMonth()-7)
            {
                Alerter("Benachrichtigung", "Die Wohnung wurde für mindestens 1 Woche gebucht. Es gibt dafür ein Rabatt von 10%.");
            }

            String writeValue = "\n"+ kundenwahl_b.getSelectionModel().getSelectedIndex() + ";" + hauswahl_b.getSelectionModel().getSelectedIndex() + ";" + Date_choose_begin.getValue().toString() + ";" + Date_choose_end.getValue().toString();
            writeFile("Buchungen.csv", writeValue);

            Alerter("Benachrichtigung", "Die Buchung war erfolgreich!\nKunde: " + kundenwahl_b.getValue());


            kundenwahl_b.setValue(null);
            hauswahl_b.setValue(null);
            Date_choose_begin.setValue(null);
            Date_choose_end.setValue(null);
            initialize();
        }
    }
    public void about(){
        Alerter("About","Ferienwohnungsverwaltung Gui\nVersion - 1.EinTagVorVorstellung\n\nMade by:\nStephan Biemann\nJulian Bischop\nSebastian Manthey\nTim Kohlsdorf\nLiam Scholtz");
    }

    public void clear_ausgabe(){
        ausgabe_area.clear();
    }

    private boolean isFree(int w_index, String startdate, String enddate) {
        boolean flag = true;
        for (int i = 1; i < getBuchung().size(); i++) {
            String s = getBuchung().get(i);
            s = s.replaceAll("\\s", "");
            String[] b = s.split(",");
            if(Integer.parseInt(b[1])==w_index){
                if ((datetodoY(startdate)>=datetodoY(b[2]) && datetodoY(startdate)<=datetodoY(b[3])) || (datetodoY(enddate)>=datetodoY(b[2])&&datetodoY(enddate)<=datetodoY(b[3]))){
                    flag = false;
                }
            }
        }
        return flag;
    }

    // File Reader
    private void readFile(String fileInfo, ArrayList<String> listTarget)
    {
        try
        {
            String readLine;
            BufferedReader Reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileInfo), Charset.forName("UTF-8")));

            while((readLine = Reader.readLine()) != null)
            {
                    listTarget.add(readLine.replace(";", ", "));
            }

            Reader.close();
        }

        catch (IOException ex)
        {
            Alerter("Fehler",fileInfo+" konnte nicht geladen werden!");
        }
    }

    // Ausgabe auslesen
    public void readAusgabe()
    {
        getHauser().clear();
        readFile("Wohnungen.csv", Main.getHauser());
        hauswahl_a.setItems(FXCollections.observableArrayList(Main.getHauser().subList(1, Main.getHauser().size())));
        readFile("Kunden.csv", Main.getKunden());
    }

    public void btn_ausgabe(){
        readAusgabeWerte(hauswahl_a.getSelectionModel().getSelectedIndex());
    }

    public void readAusgabeWerte(int w_index)
    {
        Boolean flag = true;
        readBuchungWerte();
        String haus, name, start, ende;
        haus = getHauser().get(w_index+1).split(",")[0];
        for(int i = 1; i <= getBuchung().subList(1,getBuchung().size()).size();i++){
            String s1 = getBuchung().get(i);
            s1 = s1.replaceAll("\\s", "");
            String[] b = s1.split(",");
            if(Integer.parseInt(b[1])==w_index){
                name = getKunden().get(Integer.parseInt(b[0].replaceAll("\\s",""))+1).split(",")[0]+", "+getKunden().get(Integer.parseInt(b[0].replaceAll("\\s",""))+1).split(",")[1];
                start = b[2];
                ende = b[3];
                ausgabe_area.setText(ausgabe_area.getText()+"Wohnung:\t"+haus+ "\nName:\t\t"+name+"\nVon:\t\t\t"+start+"\nBis:\t\t\t"+ende+"\n\n"+"------------------------\n\n");
                flag = false;

            }


        }
        if(flag && !ausgabe_area.getText().contains("Nichts gefunden")){
            ausgabe_area.setText(ausgabe_area.getText()+"Nichts gefunden");
        }

    }

    public String nameNachIndex(int index){
        String name = getKunden().subList(1,getKunden().size()).get(index).split(",")[1];
        return name;
    }

    // Belegung auslesen
    public void readBelegung()
    {
        getHauser().clear();
        readFile("Wohnungen.csv", Main.getHauser());
        hauswahl_n.setItems(FXCollections.observableArrayList(Main.getHauser().subList(1, Main.getHauser().size())));
    }

    // Buchungen auslesen
    public void readBuchungen()
    {
        getKunden().clear();
        getHauser().clear();
        Date_choose_begin.setValue(null);
        Date_choose_end.setValue(null);

        readFile("Kunden.csv", getKunden());
        kundenwahl_b.setItems(FXCollections.observableArrayList(getKunden().subList(1, getKunden().size())));
        readFile("Wohnungen.csv", getHauser());
        hauswahl_b.setItems(FXCollections.observableArrayList(getHauser().subList(1, getHauser().size())));
    }

    // Wohnungen auslesen
    public void readWohnungen()
    {
        getHauser().clear();
        readFile("Wohnungen.csv", Main.getHauser());
        hauswahl_g.setItems(FXCollections.observableArrayList(getHauser().subList(1, getHauser().size())));
    }

    // Wohnungswerte auslesen
    public void readWohnungenWerte()
    {
        int wohnungsIndex = hauswahl_g.getSelectionModel().getSelectedIndex();
        if (wohnungsIndex != -1) {
            wohnung_adresse.setText(getHauser().get(wohnungsIndex+1).split(",")[0]);
            wohnung_flaeche.setText(getHauser().get(wohnungsIndex+1).split(",")[1].replaceAll("\\s",""));
            wohnung_preis.setText(getHauser().get(wohnungsIndex+1).split(",")[2].replaceAll("\\s",""));
        }
    }


}